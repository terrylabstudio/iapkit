#import "Kiwi.h"
#import "IAPHelper.h"

static SKPaymentQueue* paymentQueueMock = nil;

@implementation SKPaymentQueue (HelperSpec)
+ (id)defaultQueue {
    return paymentQueueMock;
}

@end

SPEC_BEGIN(HelperSpec)

describe(@"IAPHelper", ^{
    __block IAPHelper* helper;
    
    beforeEach(^{
        helper = [[IAPHelper alloc] init];
        paymentQueueMock = [SKPaymentQueue nullMock];
    });
    
    afterEach(^{
        paymentQueueMock = nil;
    });
        
    context(@"when requestProduct", ^{
        
        __block id requestMock;
        __block id requestFactoryMock;
        __block NSSet* productIdentifiers = [NSSet setWithObject:@"just.product"];
        
        beforeEach(^{
            requestMock = [SKProductsRequest nullMock];
            requestFactoryMock = [IAPRequestFactory mock];
            [requestFactoryMock stub:@selector(requestWithProductIdentifiers:) andReturn:requestMock];
            
            helper.requestFactory = requestFactoryMock;
        });
        
        it(@"should create a request with product id", ^{
            [[requestFactoryMock should] receive:@selector(requestWithProductIdentifiers:) withArguments:productIdentifiers];
            [helper requestProducts:productIdentifiers];
        });
        
        it(@"should set itself as a delegate to request", ^{
            [[requestMock should] receive:@selector(setDelegate:) withArguments:helper];
            [helper requestProducts:productIdentifiers];
        });
        
        it(@"should be able to run a request", ^{
            [[requestMock should] receive:@selector(start)];
            [helper requestProducts:productIdentifiers];
        });
        
    });
    
//    context(@"when buyProduct", ^{
//        __block IAPHelper* helper;
//        __block id paymentFactoryMock;
//        __block id paymentMock;
//        __block id productMock;
//        beforeEach(^{
//            paymentMock = [SKPayment nullMock];
//            paymentFactoryMock = [IAPPaymentFactory mock];
//            [paymentFactoryMock stub:@selector(paymentWithProduct:) andReturn:paymentMock];
//            productMock = [SKProduct mock];
//            [productMock stub:@selector(productIdentifier) andReturn:@"unspecified"];
//            
//            helper = [[IAPHelper alloc] init];
//            helper.paymentFactory = paymentFactoryMock;
//        });
//        
//        afterEach(^{
//            paymentQueueMock = nil;
//        });
//        
//        it(@"should create payment with product", ^{
//            [[paymentFactoryMock should] receive:@selector(paymentWithProduct:) withArguments:productMock];
//            [helper buyProduct:productMock];
//        });
//        
//        it(@"should add payment", ^{
//            paymentQueueMock = [SKPaymentQueue nullMock];
//            [[paymentQueueMock should] receive:@selector(addPayment:) withArguments:paymentMock];
//            [helper buyProduct:productMock];
//        });
//    });
    
});

SPEC_END