//
//  LOLHelper.h
//  LOLTest
//
//  Created by Pavel Plevako on 11/21/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoreKit/StoreKit.h"
#import "IAPRequestFactory.h"
#import "IAPPaymentFactory.h"

#define kProductsLoadedNotification         @"ProductsLoaded"
#define kProductsFailedToLoadNotification   @"ProductsFailedToLoad"
#define kProductPurchasedNotification       @"ProductPurchased"
#define kProductPurchaseFailedNotification  @"ProductPurchaseFailed"
#define kProductsRestored                   @"ProductsRestored"
//#define kProductPurchaseCancelledNotification  @"ProductPurchaseCancelled"

@interface IAPHelper : NSObject<SKProductsRequestDelegate, SKPaymentTransactionObserver>

//@property (atomic,retain) InAppPurchaseRequestDelegate* requestDelegate;
//@property (atomic,retain) id<SKPaymentTransactionObserver> paymentObserver;
//@property (atomic,retain) SKProductsRequest* request;

@property (atomic,retain) IAPRequestFactory *requestFactory;
@property (atomic,retain) IAPPaymentFactory *paymentFactory;

- (void)requestProducts:(NSSet*) productIdentifiers;
- (void)buyProduct:(SKProduct*) product;
- (void)restoreProducts;

@end