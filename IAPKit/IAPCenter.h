//
//  IAPCenter.h
//  LOLTest
//
//  Created by Pavel Plevako on 11/26/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IAPHelper.h"

@interface IAPCenter : NSObject {
    BOOL productsRequested;
    NSMutableArray *pendingIdentifiers;
    NSSet *productIdentifiers;
}

@property (retain,readonly) NSArray *products;
@property (atomic,retain) IAPHelper *helper;

+ (IAPCenter*) sharedCenter;

- (id)initWithProductIdentifiers:(NSSet*)productIdentifiers;

- (void)requestProducts;
- (void)buyProduct:(NSString*)productIdentifier;
- (void)restoreProducts;

@end
