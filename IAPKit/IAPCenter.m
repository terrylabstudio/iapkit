//
//  IAPCenter.m
//  LOLTest
//
//  Created by Pavel Plevako on 11/26/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import "IAPCenter.h"

@interface IAPCenter ()

@property (retain, readwrite) NSArray *products;

@end

static IAPCenter *sharedCenter = nil;

@implementation IAPCenter

- (id)initWithProductIdentifiers:(NSSet*)_productIdentifiers
{
    self = [super init];
    
    if (self != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsLoaded:) name:kProductsLoadedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsFailedToLoad:) name:kProductsFailedToLoadNotification object:nil];
        self.helper = [[IAPHelper alloc] init];
        pendingIdentifiers = [NSMutableArray array];
        productIdentifiers = _productIdentifiers;
        sharedCenter = self;
    }
    
    return self;
}

+ (IAPCenter*) sharedCenter
{
    return sharedCenter;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)productsLoaded:(NSNotification *)notification
{
    _products = (NSArray*) notification.object;
    for (NSString *productIdentifier in pendingIdentifiers)
        [self buyPendingProduct:productIdentifier];
    [pendingIdentifiers removeAllObjects];
}

- (void)productsFailedToLoad:(NSNotification *)notification
{
    productsRequested = NO;
}

- (void)requestProducts
{
    if (_products)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductsLoadedNotification object:self.products];
        return;
    }
    if (productsRequested)
        return;
    productsRequested = YES;
    [_helper requestProducts:productIdentifiers];
}

- (void)buyProduct:(NSString*)productIdentifier
{
    [pendingIdentifiers addObject:productIdentifier];
    [self requestProducts];
}

- (void)restoreProducts
{
    [_helper restoreProducts];
}

- (void)buyPendingProduct:(NSString*)productIdentifier
{
    for (SKProduct *product in _products)
    {
        if ([product.productIdentifier isEqualToString:productIdentifier])
        {
            [self.helper buyProduct:product];
            return;
        }
    }
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:@"Non-existant product identifier used. IDENTIFY YOURSELF! EXTERMINATE!" forKey:NSLocalizedDescriptionKey];
    NSError *error = [NSError errorWithDomain:@"IAPCenter" code:42 userInfo:details];
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:error];
}

@end
