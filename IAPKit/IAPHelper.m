//
//  LOLHelper.m
//  LOLTest
//
//  Created by Pavel Plevako on 11/21/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import "IAPHelper.h"
#import "IAPProduct.h"

@implementation IAPHelper

- (id) init {
    self = [super init];
    
    if (self != nil)
    {
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        _requestFactory = [[IAPRequestFactory alloc] init];
        _paymentFactory = [[IAPPaymentFactory alloc] init];
    }
    
    return self;
}

- (void)dealloc
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)requestProducts:(NSSet*) productIdentifiers
{
    SKRequest *request = [_requestFactory requestWithProductIdentifiers:productIdentifiers];
    request.delegate = self;
    [request start];
}

- (void)buyProduct:(SKProduct*) product
{
    SKPayment *payment = [_paymentFactory paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

-(void)restoreProducts
{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    if (response.products.count == 0)
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductsFailedToLoadNotification object:nil];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductsLoadedNotification object:response.products];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsFailedToLoadNotification object:error];
}

- (void)provideContent:(SKPaymentTransaction *)transaction
{
    IAPProduct *product = [[IAPProduct alloc] initWithProductIdentifier:transaction.payment.productIdentifier count:transaction.payment.quantity];
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchasedNotification object:product];
    
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    [self provideContent: transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    
    //    if (transaction.error.code == SKErrorPaymentCancelled)
    //    {
    //        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    //        [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseCancelledNotification object:transaction];
    //    }
    //    else {
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:transaction.error];
    //    }
    
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"restoreTransaction...");
    
    //[self recordTransaction: transaction];
    [self provideContent: transaction.originalTransaction];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
        }
        if (transaction.transactionState != SKPaymentTransactionStatePurchasing)
            [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:error];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsRestored object:nil];
}

@end
