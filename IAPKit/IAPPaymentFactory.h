//
//  SKPaymentFactory.h
//  LOLTest
//
//  Created by Pavel Plevako on 11/22/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoreKit/StoreKit.h"

@interface IAPPaymentFactory : NSObject
- (SKPayment*)paymentWithProduct:(SKProduct*)product;
@end

