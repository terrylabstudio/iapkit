//
//  SKPaymentFactory.m
//  LOLTest
//
//  Created by Pavel Plevako on 11/22/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import "IAPPaymentFactory.h"

@implementation IAPPaymentFactory
- (SKPayment*)paymentWithProduct:(SKProduct*)product {
    return [SKPayment paymentWithProduct:product];
}
@end
