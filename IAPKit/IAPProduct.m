//
//  PurchasedProduct.m
//  LOLTest
//
//  Created by Pavel Plevako on 11/26/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import "IAPProduct.h"

@interface IAPProduct ()

@property (retain,readwrite) NSString* productIdentifier;
@property (assign,readwrite) NSInteger count;

@end


@implementation IAPProduct

- (id)initWithProductIdentifier:(NSString*)productIdentifier count:(NSInteger) count {
    self = [super init];
    
    if (self != nil)
    {
        self.productIdentifier = productIdentifier;
        self.count = count;
    }
    
    return self;
}

@end
