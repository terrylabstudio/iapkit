//
//  PurchasedProduct.h
//  LOLTest
//
//  Created by Pavel Plevako on 11/26/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IAPProduct : NSObject
@property (retain,readonly) NSString* productIdentifier;
@property (assign,readonly) NSInteger count;

- (id)initWithProductIdentifier:(NSString*)productIdentifier count:(NSInteger) count;

@end
