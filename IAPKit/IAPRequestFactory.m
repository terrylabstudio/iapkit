//
//  SKProductsRequestFactory.m
//  LOLTest
//
//  Created by Pavel Plevako on 11/22/12.
//  Copyright (c) 2012 Pavel Plevako. All rights reserved.
//

#import "IAPRequestFactory.h"

@implementation IAPRequestFactory
- (SKProductsRequest*)requestWithProductIdentifiers:(NSSet *)productIdentifiers {
    return [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
}
@end